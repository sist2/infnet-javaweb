package br.edu.infnet.appveiculos.repository;

import org.springframework.data.repository.CrudRepository;

import br.edu.infnet.appveiculos.model.domain.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
	
	public Usuario findByEmail(String email);
	
}
