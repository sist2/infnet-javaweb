package br.edu.infnet.appveiculos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.edu.infnet.appveiculos.model.domain.Veiculo;

@Repository
public interface VeiculoRepository extends CrudRepository<Veiculo, Integer> {
	
	@Query("from Veiculo p where p.usuario.id = :userid")
	public List<Veiculo> obterLista(Integer userid);
}
