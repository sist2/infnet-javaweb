package br.edu.infnet.appveiculos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.edu.infnet.appveiculos.model.domain.Eletrico;

public interface EletricoRepository extends CrudRepository<Eletrico, Integer> {
	
	@Query("from Eletrico p where p.usuario.id = :userid")
	public List<Eletrico> obterLista(Integer userid);
}
