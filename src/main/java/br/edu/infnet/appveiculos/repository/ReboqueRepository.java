package br.edu.infnet.appveiculos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.edu.infnet.appveiculos.model.domain.Reboque;

public interface ReboqueRepository extends CrudRepository<Reboque, Integer> {
	
	@Query("from Reboque p where p.usuario.id = :userid")
	public List<Reboque> obterLista(Integer userid);
	
}
