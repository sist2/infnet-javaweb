package br.edu.infnet.appveiculos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.edu.infnet.appveiculos.model.domain.Locacao;

public interface LocacaoRepository extends CrudRepository<Locacao, Integer> {
	
	@Query("from Locacao p where p.usuario.id = :userid")
	public List<Locacao> obterLista(Integer userid);
	
}
