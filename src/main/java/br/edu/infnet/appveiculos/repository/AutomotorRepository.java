package br.edu.infnet.appveiculos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.edu.infnet.appveiculos.model.domain.Automotor;

@Repository
public interface AutomotorRepository extends CrudRepository<Automotor, Integer>{
	
	@Query("from Automotor b where b.usuario.id = :userid")
	public List<Automotor> obterLista(Integer userid);
	
}
