package br.edu.infnet.appveiculos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.edu.infnet.appveiculos.model.domain.Automotor;
import br.edu.infnet.appveiculos.model.domain.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	
	@Query("from Cliente b where b.usuario.id = :userid")
	public List<Cliente> obterLista(Integer userid);
}
