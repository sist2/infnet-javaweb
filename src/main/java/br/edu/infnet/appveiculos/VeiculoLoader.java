package br.edu.infnet.appveiculos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.edu.infnet.appveiculos.model.domain.Automotor;
import br.edu.infnet.appveiculos.model.domain.Eletrico;
import br.edu.infnet.appveiculos.model.domain.Reboque;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.service.VeiculoService;


@Order(3)
@Component
public class VeiculoLoader implements ApplicationRunner{
	
	@Autowired
	private VeiculoService veiculoService;

	@Override
	public void run(ApplicationArguments args) throws Exception {

		FileReader file = new FileReader("arquivos/veiculo.txt");
		BufferedReader leitura = new BufferedReader(file);
		
		String linha = leitura.readLine();
		
		String[] campos = null;
		
		while(linha != null) {
			campos = linha.split(";"); 
			
			switch (campos[7]) {
			case "A":
				Automotor automotor = new Automotor(
						campos[0], 
						new BigDecimal(campos[1]),
						Integer.valueOf(campos[2]), 
						Boolean.valueOf(campos[3]), 
						Integer.valueOf(campos[4]), 
						Integer.valueOf(campos[5])
					);	
				
				automotor.setUsuario(new Usuario(Integer.valueOf(campos[6])));

				veiculoService.incluir(automotor);				
				break;

			case "E":
				Eletrico eletrico = new Eletrico(
						campos[0], 
						new BigDecimal(campos[1]),
						Integer.valueOf(campos[2]), 
						campos[3],
						new BigDecimal(campos[4]),
						Boolean.valueOf(campos[5])						
					);
				
				eletrico.setUsuario(new Usuario(Integer.valueOf(campos[6])));

				veiculoService.incluir(eletrico);
				break;

			case "R":
				Reboque reboque = new Reboque(
						campos[0], 
						new BigDecimal(campos[1]),
						Integer.valueOf(campos[2]), 
						Integer.valueOf(campos[3]), 
						campos[4], 
						new BigDecimal(campos[5])
					);
				
				reboque.setUsuario(new Usuario(Integer.valueOf(campos[6])));

				veiculoService.incluir(reboque);
				break;
			
			default:
				break;
			}
			
			linha = leitura.readLine();
		}

		leitura.close();		
	}
	
}
