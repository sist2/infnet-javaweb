package br.edu.infnet.appveiculos.model.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Teletrico")
public class Eletrico extends Veiculo{
	
	private String marca;
	
	private BigDecimal custoRevisao;
	
	private boolean ehSistemaRegenerativo;
	
	public Eletrico() {

	}
	
	public Eletrico(Integer id) {
		super(id);
	}
	
	public Eletrico(String placa, BigDecimal valorDia, Integer anoModelo, String marca, BigDecimal custoRevisao, boolean ehSistemaRegenerativo) {
		super(placa, valorDia, anoModelo);
		this.marca = marca;
		this.custoRevisao = custoRevisao;
		this.ehSistemaRegenerativo = ehSistemaRegenerativo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public BigDecimal getCustoRevisao() {
		return custoRevisao;
	}

	public void setCustoRevisao(BigDecimal custoRevisao) {
		this.custoRevisao = custoRevisao;
	}

	public boolean isEhSistemaRegenerativo() {
		return ehSistemaRegenerativo;
	}

	public void setEhSistemaRegenerativo(boolean ehSistemaRegenerativo) {
		this.ehSistemaRegenerativo = ehSistemaRegenerativo;
	}
	
	
}
