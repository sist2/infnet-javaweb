package br.edu.infnet.appveiculos.model.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Tautomotor")
public class Automotor extends Veiculo{
	
	private boolean temAirbag;
	
	private Integer kilometragem;
	
	private Integer quantidadePassageiros;
	
	public Automotor() {
		
	}
	
	public Automotor(Integer id) {
		super(id);
	}
	
	public Automotor(String placa, BigDecimal valorDia, Integer anoModelo, boolean temAirbag, Integer kilometragem, Integer quantidadePassageiros) {
		super(placa, valorDia, anoModelo);
		this.temAirbag = temAirbag;
		this.kilometragem = kilometragem;
		this.quantidadePassageiros = quantidadePassageiros;
	}

	public boolean isTemAirbag() {
		return temAirbag;
	}

	public void setTemAirbag(boolean temAirbag) {
		this.temAirbag = temAirbag;
	}

	public Integer getKilometragem() {
		return kilometragem;
	}

	public void setKilometragem(Integer kilometragem) {
		this.kilometragem = kilometragem;
	}

	public Integer getQuantidadePassageiros() {
		return quantidadePassageiros;
	}

	public void setQuantidadePassageiros(Integer quantidadePassageiros) {
		this.quantidadePassageiros = quantidadePassageiros;
	}

	@Override
	public String toString() {
		return "Automotor [temAirbag=" + temAirbag + ", kilometragem=" + kilometragem + ", quantidadePassageiros="
				+ quantidadePassageiros + "]";
	}
	
	
	
}
