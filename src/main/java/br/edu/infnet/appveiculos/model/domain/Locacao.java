package br.edu.infnet.appveiculos.model.domain;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Tlocacao")
public class Locacao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String descricao;
	
	private Integer dias;
	
	private LocalDateTime dataLocacao;
	
	@OneToOne(cascade = CascadeType.DETACH) 
	@JoinColumn(name = "idCliente")
	private Cliente cliente;
	
	@ManyToMany(cascade = CascadeType.DETACH)
	private List<Veiculo> veiculos;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario")
	private Usuario usuario;
	
	public Locacao() {
		descricao = "Pedido inicial";
		dataLocacao = LocalDateTime.now();
		dias = 7;
	}

	public Locacao(Cliente cliente) {
		this();
		this.cliente = cliente;
	}
	
	public Locacao(String descricao, Integer dias, LocalDateTime dataLocacao, Cliente cliente, List<Veiculo> veiculos, Usuario usuario) {
		super();
		this.descricao = descricao;
		this.dias = dias;
		this.dataLocacao = dataLocacao;
		this.cliente = cliente;
		this.veiculos = veiculos;
		this.usuario = usuario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setNome(String descricao) {
		this.descricao = descricao;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public LocalDateTime getDataLocacao() {
		return dataLocacao;
	}

	public void setDataLocacao(LocalDateTime dataLocacao) {
		this.dataLocacao = dataLocacao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Locacao(Cliente cliente, List<Veiculo> veiculos) {
		dataLocacao = LocalDateTime.now();
		dias = 10;
		descricao = "Primeira locação";
		this.cliente = cliente;
		this.veiculos = veiculos;
	}

	@Override
	public String toString() {
		return "Locacao [id=" + id + ", descricao=" + descricao + ", dias=" + dias + ", dataLocacao=" + dataLocacao
				+ ", cliente=" + cliente + ", veiculos=" + veiculos + ", usuario=" + usuario + "]";
	}

	
	
	
	
	

	
	
	
	
}
