package br.edu.infnet.appveiculos.model.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Treboque")
public class Reboque extends Veiculo{
	
	private Integer quantidadeEixos;
	
	private String motor;
	
	private BigDecimal comprimento;

	public Reboque() {
		
	}
	
	public Reboque(Integer id) {
		super(id);
	}
	
	public Reboque(String placa, BigDecimal valorDia, Integer anoModelo, Integer quantidadeEixos, String motor, BigDecimal comprimento) {
		super(placa, valorDia, anoModelo);
		this.quantidadeEixos = quantidadeEixos;
		this.motor = motor;
		this.comprimento = comprimento;
	}

	public Integer getQuantidadeEixos() {
		return quantidadeEixos;
	}

	public void setQuantidadeEixos(Integer quantidadeEixos) {
		this.quantidadeEixos = quantidadeEixos;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public BigDecimal getComprimento() {
		return comprimento;
	}

	public void setComprimento(BigDecimal comprimento) {
		this.comprimento = comprimento;
	}
	
	
}
