package br.edu.infnet.appveiculos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import br.edu.infnet.appveiculos.model.domain.Eletrico;
import br.edu.infnet.appveiculos.model.domain.Reboque;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.service.EletricoService;
import br.edu.infnet.appveiculos.service.ReboqueService;

@Controller
public class ReboqueController {
	
	@Autowired
	private ReboqueService reboqueService;

	@GetMapping(value = "/reboque/lista")
	public String telaLista(Model model, @SessionAttribute("user") Usuario usuario) {

		model.addAttribute("listaReboque", reboqueService.obterLista(usuario));
		
		return "reboque/lista";
	}

	@GetMapping(value = "/reboque/cadastro")
	public String telaCadastro() {

		return "reboque/cadastro";
	}

	@PostMapping(value = "/reboque/incluir")
	public String incluir(Reboque reboque, @SessionAttribute("user") Usuario usuario) {
		
		reboque.setUsuario(usuario);
		
		reboqueService.incluir(reboque);
		
		return "redirect:/reboque/lista";
	}
	
	@GetMapping(value = "/reboque/{id}/excluir")
	public String exclusao(@PathVariable Integer id) {
		
		reboqueService.excluir(id);

		return "redirect:/reboque/lista";
	}
}
