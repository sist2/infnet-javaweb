package br.edu.infnet.appveiculos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import br.edu.infnet.appveiculos.model.domain.Eletrico;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.service.EletricoService;

@Controller
public class EletricoController {
	
	@Autowired
	private EletricoService eletricoService;

	@GetMapping(value = "/eletrico/lista")
	public String telaLista(Model model, @SessionAttribute("user") Usuario usuario) {

		model.addAttribute("listaEletrico", eletricoService.obterLista(usuario));
		
		return "eletrico/lista";
	}

	@GetMapping(value = "/eletrico/cadastro")
	public String telaCadastro() {

		return "eletrico/cadastro";
	}

	@PostMapping(value = "/eletrico/incluir")
	public String incluir(Eletrico eletrico, @SessionAttribute("user") Usuario usuario) {
		
		eletrico.setUsuario(usuario);
		
		eletricoService.incluir(eletrico);
		
		return "redirect:/eletrico/lista";
	}
	
	@GetMapping(value = "/eletrico/{id}/excluir")
	public String exclusao(@PathVariable Integer id) {
		
		eletricoService.excluir(id);

		return "redirect:/eletrico/lista";
	}
	
}
