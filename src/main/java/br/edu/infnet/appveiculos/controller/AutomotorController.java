package br.edu.infnet.appveiculos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import br.edu.infnet.appveiculos.model.domain.Automotor;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.service.AutomotorService;

@Controller
public class AutomotorController {
	
	@Autowired
	private AutomotorService automotorService;

	@GetMapping(value = "/automotor/lista")
	public String telaLista(Model model, @SessionAttribute("user") Usuario usuario) {

		model.addAttribute("listaAutomotor", automotorService.obterLista(usuario));
		
		return "automotor/lista";
	}

	@GetMapping(value = "/automotor/cadastro")
	public String telaCadastro() {

		return "automotor/cadastro";
	}

	@PostMapping(value = "/automotor/incluir")
	public String incluir(Automotor automotor, @SessionAttribute("user") Usuario usuario) {
		
		automotor.setUsuario(usuario);
		
		automotorService.incluir(automotor);
		
		return "redirect:/automotor/lista";
	}
	
	@GetMapping(value = "/automotor/{id}/excluir")
	public String exclusao(@PathVariable Integer id) {
		
		automotorService.excluir(id);

		return "redirect:/automotor/lista";
	}
	
}
