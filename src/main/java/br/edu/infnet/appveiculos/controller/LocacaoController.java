package br.edu.infnet.appveiculos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import br.edu.infnet.appveiculos.model.domain.Locacao;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.service.ClienteService;
import br.edu.infnet.appveiculos.service.LocacaoService;
import br.edu.infnet.appveiculos.service.VeiculoService;

@Controller
public class LocacaoController {
	
	@Autowired
	private LocacaoService locacaoService;
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private VeiculoService veiculoService;
	
	@GetMapping(value = "/locacao")
	public String telaCadastro(Model model, @SessionAttribute("user") Usuario usuario) {

		model.addAttribute("cliente", clienteService.obterLista(usuario));

		model.addAttribute("veiculos", veiculoService.obterLista(usuario));
		
		return "locacao/cadastro";
	}
	
	@GetMapping(value = "/locacao/lista")
	public String telaLista(Model model, @SessionAttribute("user") Usuario usuario) {
		model.addAttribute("listagem", locacaoService.obterLista(usuario));

		return "locacao/lista";
	}
	
	@PostMapping(value = "/locacao/incluir")
	public String incluir(Locacao locacao, @SessionAttribute("user") Usuario usuario) {
		
		locacao.setUsuario(usuario);
		
		locacaoService.incluir(locacao);
		
		return "redirect:/locacao/lista";
	}

	@GetMapping(value = "/locacao/{id}/excluir")
	public String excluir(@PathVariable Integer id) {

		locacaoService.excluir(id);
		
		return "redirect:/locacao/lista";
	}
	
}
