package br.edu.infnet.appveiculos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppveiculosApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppveiculosApplication.class, args);
	}

}
