	package br.edu.infnet.appveiculos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.infnet.appveiculos.model.domain.Locacao;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.repository.LocacaoRepository;

@Service
public class LocacaoService {
	
	@Autowired
	private LocacaoRepository locacaoRepository;
	
	public Collection<Locacao> obterLista(){
		
		return (Collection<Locacao>) locacaoRepository.findAll();
	}
	
	public Collection<Locacao> obterLista(Usuario usuario){
		
		return (Collection<Locacao>) locacaoRepository.obterLista(usuario.getId());
	}

	public void incluir(Locacao locacao) {

		locacaoRepository.save(locacao);
	}
	
	public void excluir(Integer id) {

		locacaoRepository.deleteById(id);
	}
}
