package br.edu.infnet.appveiculos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.infnet.appveiculos.model.domain.Eletrico;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.repository.EletricoRepository;

@Service
public class EletricoService {
	
	@Autowired
	private EletricoRepository eletricoRepository;
	
	public Collection<Eletrico> obterLista(){
		
		return (Collection<Eletrico>) eletricoRepository.findAll();
	}
	
	public Collection<Eletrico> obterLista(Usuario usuario){
		
		return (Collection<Eletrico>) eletricoRepository.obterLista(usuario.getId());
	}

	public void incluir(Eletrico eletrico) {

		eletricoRepository.save(eletrico);
	}
	
	public void excluir(Integer id) {

		eletricoRepository.deleteById(id);
	}
}
