package br.edu.infnet.appveiculos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.model.domain.Veiculo;
import br.edu.infnet.appveiculos.repository.VeiculoRepository;

@Service
public class VeiculoService {
	
	@Autowired
	private VeiculoRepository veiculoRepository;
	
	public Collection<Veiculo> obterLista(){
		
		return (Collection<Veiculo>) veiculoRepository.findAll();
	}
	
	public Collection<Veiculo> obterLista(Usuario usuario){
		
		return (Collection<Veiculo>) veiculoRepository.obterLista(usuario.getId());
	}

	public void incluir(Veiculo veiculo) {

		veiculoRepository.save(veiculo);
	}
	
	public void excluir(Integer id) {

		veiculoRepository.deleteById(id);
	}	
}
