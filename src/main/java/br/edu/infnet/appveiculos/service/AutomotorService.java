package br.edu.infnet.appveiculos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.infnet.appveiculos.model.domain.Automotor;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.repository.AutomotorRepository;

@Service
public class AutomotorService {
	
	@Autowired
	private AutomotorRepository automotorRepository;
	
	public Collection<Automotor> obterLista(){
		
		return (Collection<Automotor>) automotorRepository.findAll();
	}
	
	public Collection<Automotor> obterLista(Usuario usuario){
		
		return (Collection<Automotor>) automotorRepository.obterLista(usuario.getId());
	}

	public void incluir(Automotor automotor) {

		automotorRepository.save(automotor);
	}
	
	public void excluir(Integer id) {

		automotorRepository.deleteById(id);
	}	
}
