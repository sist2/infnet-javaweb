package br.edu.infnet.appveiculos.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.infnet.appveiculos.model.domain.Reboque;
import br.edu.infnet.appveiculos.model.domain.Usuario;
import br.edu.infnet.appveiculos.repository.ReboqueRepository;

@Service
public class ReboqueService {
	
	@Autowired
	private ReboqueRepository reboqueRepository;
	
	public Collection<Reboque> obterLista(){
		
		return (Collection<Reboque>) reboqueRepository.findAll();
	}
	
	public Collection<Reboque> obterLista(Usuario usuario){
		
		return (Collection<Reboque>) reboqueRepository.obterLista(usuario.getId());
	}

	public void incluir(Reboque reboque) {

		reboqueRepository.save(reboque);
	}
	
	public void excluir(Integer id) {

		reboqueRepository.deleteById(id);
	}
}
