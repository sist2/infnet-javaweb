<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>AppVeiculos</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<div class="container mt-3">
  <h2>Classes de Dom�nio</h2>
  <p>Ve�culos</p>            
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Campo</th>
        <th>Tipo</th>
        <th>Observa��o</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>descricao</td>
        <td>String</td>
        <td>Detalhe pedido</td>
      </tr>
      <tr>
        <td>data</td>
        <td>LocalDateTime</td>
        <td>Data de realiza��o do pedido</td>
      </tr>
      <tr>
        <td>web</td>
        <td>boolean</td>
        <td>Indicativo pedido realizado presencial ou web</td>
      </tr>
    </tbody>
  </table>
</div>
</body>
</html>