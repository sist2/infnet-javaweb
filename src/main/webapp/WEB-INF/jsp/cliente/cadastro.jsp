<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<title>Cadastro de Cliente</title>
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>

	<div class="container mt-3">

	<h2>Cliente</h2>
	  <form action="/cliente/incluir" method="post">
	    <div class="form-group">
	      <label>Nome:</label>
	      <input type="text" class="form-control" placeholder="Entre com o seu nome" name="nome">
	    </div>
	    <div class="form-group">
	      <label>Telefone:</label>
	      <input type="text" class="form-control" placeholder="Entre com o telefone" name="telefone">
	    </div>
	    <div class="form-group">
	      <label>Endere�o:</label>
	      <input type="text" class="form-control" placeholder="Entre com o seu endere�o" name="endereco">
	    </div>
	    <div class="form-group">
	      <label>Identidade:</label>
	      <input type="text" class="form-control" placeholder="Entre com a sua identidade" name="identidade">
	    </div>

	    <button type="submit" class="btn btn-default">Cadastrar</button>
	  </form>
  
	</div>
</body>
</html>