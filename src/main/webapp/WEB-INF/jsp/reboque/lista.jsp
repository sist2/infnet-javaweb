<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>AppVeiculo</title>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>
	
	<div class="container-fluid mt-3">
	  <h3>Listagem de Reboques</h3>
	  <h4><a href="/reboque/cadastro">Novo Reboque</a></h4>

	  <table class="table table-striped">
	    <thead>
	      <tr>
	        <th>ID</th>
	        <th>Placa</th>
	        <th>Valor Dia</th>
	        <th>Ano Modelo</th>
	        <th>Quantidade Eixos</th>
	        <th>Motor</th>
	        <th>Comprimento</th>
	        <th></th>
	      </tr>
	    </thead>
	    <tbody>
	   	    
	      <c:forEach var="r" items="${listaReboque}">
		      <tr>
		        <td>${r.id}</td>
		        <td>${r.placa}</td>
		        <td>${r.valorDia}</td>
		        <td>${r.anoModelo}</td>
		        <td>${r.quantidadeEixos}</td>
		        <td>${r.motor}</td>
		        <td>${r.comprimento}</td>
		        <td><a href="/reboque/${r.id}/excluir">excluir</a> </td>
		      </tr>
	      </c:forEach>
	    </tbody>
	  </table>
	  
	</div>
</body>
</html>