<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<title>Cadastro de Reboque</title>
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>

	<div class="container mt-3">

	<h2>Automotor</h2>
	  <form action="/reboque/incluir" method="post">
	    <div class="form-group">
	      <label>Placa:</label>
	      <input type="text" class="form-control" placeholder="Entre com a placa" name="placa">
	    </div>
	    <div class="form-group">
	      <label>Valor do Dia:</label>
	      <input type="text" class="form-control" placeholder="Entre com o valor do dia" name="valorDia">
	    </div>
	    <div class="form-group">
	      <label>Ano Modelo</label>
	      <input type="text" class="form-control" placeholder="Entre com o ano do modelo" name="anoModelo">
	    </div>
	    <div class="form-group">
	      <label>Quantidade de eixos</label>
	      <input type="text" class="form-control" placeholder="Entre com a quantidade de eixos" name="quantidadeEixos">
	    </div>	
		<div class="form-group">
	      <label>Motor</label>
	      <input type="text" class="form-control" placeholder="Entre com o motor" name="motor">
	    </div>
	    <div class="form-group">
	      <label>Comprimento</label>
	      <input type="text" class="form-control" placeholder="Entre com o comprimento do ve�culo" name="comprimento">
	    </div>

	    <button type="submit" class="btn btn-default">Cadastrar</button>
	  </form>
  
	</div>
</body>
</html>