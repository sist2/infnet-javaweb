<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>AppVeiculo</title>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>
	
	<div class="container-fluid mt-3">
	  <h3>Listagem de Eletricos</h3>
	  <h4><a href="/eletrico/cadastro">Novo El�trico</a></h4>

	  <table class="table table-striped">
	    <thead>
	      <tr>
	        <th>ID</th>
	        <th>Placa</th>
	        <th>Valor Dia</th>
	        <th>Ano Modelo</th>
	        <th>Marca</th>
	        <th>Custo Revis�o</th>
	        <th>Sistema Regenerativo?</th>
	        <th></th>
	      </tr>
	    </thead>
	    <tbody>
	   	    
	      <c:forEach var="e" items="${listaEletrico}">
		      <tr>
		        <td>${e.id}</td>
		        <td>${e.placa}</td>
		        <td>${e.valorDia}</td>
		        <td>${e.anoModelo}</td>
		        <td>${e.marca}</td>
		        <td>${e.custoRevisao}</td>
		        <td>${e.ehSistemaRegenerativo}</td>
		        <td><a href="/eletrico/${e.id}/excluir">excluir</a> </td>
		      </tr>
	      </c:forEach>
	    </tbody>
	  </table>
	  
	</div>
</body>
</html>