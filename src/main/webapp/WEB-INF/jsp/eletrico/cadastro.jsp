<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<title>Cadastro de Eletrico</title>
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>

	<div class="container mt-3">

	<h2>Eletrico</h2>
	  <form action="/eletrico/incluir" method="post">
	    <div class="form-group">
	      <label>Placa:</label>
	      <input type="text" class="form-control" placeholder="Entre com a placa" name="placa">
	    </div>
	    <div class="form-group">
	      <label>Valor do Dia:</label>
	      <input type="text" class="form-control" placeholder="Entre com o valor do dia" name="valorDia">
	    </div>
	    <div class="form-group">
	      <label>Ano Modelo</label>
	      <input type="text" class="form-control" placeholder="Entre com o ano do modelo" name="anoModelo">
	    </div>
	    <div class="form-group">
	      <label>Marca</label>
	      <input type="text" class="form-control" placeholder="Entre com a marca" name="marca">
	    </div>	
		<div class="form-group">
	      <label>Custo Revis�o</label>
	      <input type="text" class="form-control" placeholder="Entre com o custo da revis�o" name="custoRevisao">
	    </div>
	    <div class="form-check">
		  <input class="form-check-input" type="checkbox" name="ehSistemaRegenerativo" value="true" checked>
		  <label class="form-check-label">Tem Sistema Regenerativo</label>
		</div>	

	    <button type="submit" class="btn btn-default">Cadastrar</button>
	  </form>
  
	</div>
</body>
</html>