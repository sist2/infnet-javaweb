<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>AppVeiculo</title>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>
	
	<div class="container-fluid mt-3">
	  <h3>Listagem de Automotores</h3>
	  <h4><a href="/automotor/cadastro">Novo Automotor</a></h4>

	  <table class="table table-striped">
	    <thead>
	      <tr>
	        <th>ID</th>
	        <th>Placa</th>
	        <th>Valor Dia</th>
	        <th>Ano Modelo</th>
	        <th>Tem airbag</th>
	        <th>Kilometragem</th>
	        <th>Quantidade Passageiros</th>
	        <th></th>
	      </tr>
	    </thead>
	    <tbody>
	   	    
	      <c:forEach var="a" items="${listaAutomotor}">
		      <tr>
		        <td>${a.id}</td>
		        <td>${a.placa}</td>
		        <td>${a.valorDia}</td>
		        <td>${a.anoModelo}</td>
		        <td>${a.temAirbag}</td>
		        <td>${a.kilometragem}</td>
		        <td>${a.quantidadePassageiros}</td>
		        <td><a href="/automotor/${a.id}/excluir">excluir</a> </td>
		      </tr>
	      </c:forEach>
	    </tbody>
	  </table>
	  
	</div>
</body>
</html>