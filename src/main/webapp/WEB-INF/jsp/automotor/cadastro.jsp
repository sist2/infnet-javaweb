<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<title>Cadastro de Automotor</title>
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>

	<div class="container mt-3">

	<h2>Automotor</h2>
	  <form action="/automotor/incluir" method="post">
	    <div class="form-group">
	      <label>Placa:</label>
	      <input type="text" class="form-control" placeholder="Entre com a placa" name="placa">
	    </div>
	    <div class="form-group">
	      <label>Valor do Dia:</label>
	      <input type="text" class="form-control" placeholder="Entre com o valor do dia" name="valorDia">
	    </div>
	    <div class="form-group">
	      <label>Ano Modelo</label>
	      <input type="text" class="form-control" placeholder="Entre com o ano do modelo" name="anoModelo">
	    </div>
	    <div class="form-check">
		  <input class="form-check-input" type="checkbox" name="temAirbag" value="true" checked>
		  <label class="form-check-label">Tem Airbag</label>
		</div>	
		<div class="form-group">
	      <label>Kilometragem</label>
	      <input type="text" class="form-control" placeholder="Entre com a kilometragem" name="kilometragem">
	    </div>
	    <div class="form-group">
	      <label>Quantidade Passageiros</label>
	      <input type="text" class="form-control" placeholder="Entre com a quantidade de passageiros do ve�culo" name="quantidadePassageiros">
	    </div>

	    <button type="submit" class="btn btn-default">Cadastrar</button>
	  </form>
  
	</div>
</body>
</html>