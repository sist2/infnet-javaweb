<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<title>Cadastro de Loca��o de Ve�culos</title>
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>

	<div class="container mt-3">

	<h2>Loca��o</h2>
	  <form action="/locacao/incluir" method="post">
	    <div class="form-group">
	      <label>Descri��o:</label>
	      <input type="text" class="form-control" placeholder="Entre com a descri��o" name="descricao">
	    </div>
	    <div class="form-group">
	      <label>Dias:</label>
	      <input type="text" class="form-control" placeholder="Entre com os dias de aluguel" name="dias">
	    </div>
	    <div class="form-group">
	      <label>Data:</label>
	      <input type="datetime-local" class="form-control" name="dtCliente">
	    </div>
	    

	    <div class="form-group">
	      <label>Cliente:</label>
	      <select name="cliente" class="form-control">
	      	<c:forEach var="c" items="${cliente}">
	      		<option value="${c.id}">${c.nome}</option>
	      	</c:forEach>
	      </select>
	    </div>

	    <div class="form-group">
	      <label>Veiculos:</label>
	      	<c:forEach var="v" items="${veiculos}">
				<div class="form-check">
				  <input class="form-check-input" type="checkbox" name="veiculos" value="${v.id}">
				  <label class="form-check-label"> ${v.placa}</label>
				</div>	   
			</c:forEach>   
	    </div>

	    <button type="submit" class="btn btn-default">Cadastrar</button>
	  </form>
  
	</div>
</body>
</html>