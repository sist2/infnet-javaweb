<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<title>Cadastro de Loca��o de Ve�culos</title>
</head>
<body>
	<c:import url="/WEB-INF/jsp/menu.jsp"/>

	<div class="container mt-3">
	  <h3>Veiculos: ${listagem.size()}</h3>

	  <h4><a href="/locacao">Nova Loca��o</a></h4>

	  <table class="table table-striped">
	    <thead>
	      <tr>
	        <th>ID</th>
	        <th>Descri��o</th>
	        <th>Dias</th>
	        <th>Data Loca��o</th>
	        <th>Cliente</th>
	        <th>Ve�culos</th>
	        <th></th>
	      </tr>
	    </thead>
	    <tbody>
		  <c:forEach var="l" items="${listagem}">
		      <tr>
				<td>${l.id}</td>
		        <td>${l.descricao}</td>
		        <td>${l.dias}</td>
		        <td>${l.dataLocacao}</td>		        
		        <td>${l.cliente.nome}</td>
		        <td>${l.veiculos.size()}</td>
		        <td><a href="/locacao/${l.id}/excluir">excluir</a></td>
		      </tr>
	      </c:forEach>
	    </tbody>
	  </table>
	</div>
</body>
</html>